import Head from "next/head"

const PageLayout = (props) => {
    return(
      <>
      <Head>
      <meta />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&display=swap" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <title>Rally App | Manage Your Finances Easily</title>

        {/* SEO */}
        <meta name="description" content="Rally App | Manage Your Finances Easily" />

        {/* OpenGraph on Facebook */}
        <meta property="og:title" content="Manage Your Finances Easily With Rally app. Now avilable on Apple and Android" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="/assets/img/banner-image.webp" />
        <meta property="og:url" content="https://https://seo-training.vercel.app/" />
        <meta property="og:description" content="Managing your finances has never been easier with Rally. With the simplicity and analytics, you can now manage anytime and anywhere" />

        {/* OpenGraph on Twitter */}
        <meta name="twitter:title" content="Manage Your Finances Easily With Rally app. Now avilable on Apple and Android" />
        <meta name="twitter:card" content="Rally App" />
        <meta name="twitter:image" content="/assets/img/banner-image.webp" />
        <meta name="twitter:description" content="Managing your finances has never been easier with Rally. With the simplicity and analytics, you can now manage anytime and anywhere" />
      </Head>

      <div>
          {props.children}
      </div>
      </>
    )
}

export default PageLayout